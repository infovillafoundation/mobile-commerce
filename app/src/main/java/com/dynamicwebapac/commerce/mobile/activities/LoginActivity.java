package com.dynamicwebapac.commerce.mobile.activities;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import android.widget.LinearLayout.LayoutParams;
import com.dynamicwebapac.commerce.mobile.R;
import com.dynamicwebapac.commerce.mobile.custom.CustomActivity;
import com.dynamicwebapac.commerce.mobile.models.LoginRequest;
import com.dynamicwebapac.commerce.mobile.models.LoginResponse;
import com.dynamicwebapac.commerce.mobile.rest.DynamicWebService;
import com.dynamicwebapac.commerce.mobile.utils.EncryptHelper;
import com.dynamicwebapac.commerce.mobile.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import retrofit.*;


public class LoginActivity extends CustomActivity {

    private ViewPager pager;
    private LinearLayout vDots;

    private EditText user;
    private EditText pwd;
    
    class C01851 implements OnPageChangeListener {
        C01851() {
        }

        public void onPageSelected(int pos) {
            if (LoginActivity.this.vDots != null && LoginActivity.this.vDots.getTag() != null) {
                ((ImageView) LoginActivity.this.vDots.getTag()).setImageResource(R.drawable.dot_gray);
                ((ImageView) LoginActivity.this.vDots.getChildAt(pos)).setImageResource(R.drawable.dot_blue);
                LoginActivity.this.vDots.setTag(LoginActivity.this.vDots.getChildAt(pos));
            }
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageScrollStateChanged(int arg0) {
        }
    }

    private class PageAdapter extends PagerAdapter {
        private PageAdapter() {
        }

        public int getCount() {
            return 6;
        }

        public Object instantiateItem(ViewGroup container, int arg0) {
            RelativeLayout imgLayout = (RelativeLayout) LoginActivity.this.getLayoutInflater().inflate(R.layout.img, null);
            ImageView img = (ImageView) imgLayout.findViewById(R.id.img);
            ImageView textImage = (ImageView) imgLayout.findViewById(R.id.text_image);
            int imgResource;
            int textImageResource;
            switch (arg0) {
                case 0: imgResource = R.drawable.img_signin;
                    textImageResource = R.drawable.experience_shopping;
                    break;
                case 1: imgResource = R.drawable.login_slide_img_2;
                    textImageResource = R.drawable.joy_of_buying;
                    break;
                case 2: imgResource = R.drawable.login_slide_img_3;
                    textImageResource = R.drawable.delight;
                    break;
                case 3: imgResource = R.drawable.login_slide_img_4;
                    textImageResource = R.drawable.fulfill;
                    break;
                case 4: imgResource = R.drawable.login_slide_img_5;
                    textImageResource = R.drawable.inspire;
                    break;
                case 5: imgResource = R.drawable.login_slide_img_6;
                    textImageResource = R.drawable.pristine_touch;
                    break;
                default: imgResource = R.drawable.img_signin;
                    textImageResource = R.drawable.experience_shopping;
                    break;

            }
            img.setImageResource(imgResource);
            textImage.setImageResource(textImageResource);
            container.addView(imgLayout, -1, -1);
            return imgLayout;
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
        }

        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setupView();

        user = (EditText) findViewById(R.id.user);
        pwd = (EditText) findViewById(R.id.pwd);
    }

    private void setupView() {
        ((Button) setTouchNClick(R.id.btnReg)).setText(Html.fromHtml(getString(R.string.sign_up)));
        setTouchNClick(R.id.btnLogin);
        setTouchNClick(R.id.btnForget);
        setTouchNClick(R.id.btnFb);
        initPager();
    }

    private void initPager() {
        this.pager = (ViewPager) findViewById(R.id.pager);
        this.pager.setPageMargin(10);
        this.pager.setOnPageChangeListener(new C01851());
        this.vDots = (LinearLayout) findViewById(R.id.vDots);
        this.pager.setAdapter(new PageAdapter());
        setupDotbar();
    }

    private void setupDotbar() {
        LayoutParams param = new LayoutParams(-2, -2);
        param.setMargins(10, 0, 0, 0);
        this.vDots.removeAllViews();
        for (int i = 0; i < 6; i++) {
            int i2;
            ImageView img = new ImageView(this);
            if (i == 0) {
                i2 = R.drawable.dot_blue;
            } else {
                i2 = R.drawable.dot_gray;
            }
            img.setImageResource(i2);
            this.vDots.addView(img, param);
            if (i == 0) {
                this.vDots.setTag(img);
            }
        }
    }

    public void onClick(View v) {
        super.onClick(v);
        if (v.getId() == R.id.btnLogin || v.getId() == R.id.btnFb) {
            String u = user.getText().toString();
            String p = pwd.getText().toString();
            if (u.length() == 0 || p.length() == 0)
            {
                Utils.showDialog(this, R.string.err_fields_empty);
                return;
            }
            final ProgressDialog dia = ProgressDialog.show(this, null,
                    getString(R.string.alert_wait));

            try {
                LoginRequest loginRequest = new LoginRequest();
                loginRequest.setUsername(EncryptHelper.encrypt(u));
                loginRequest.setPassword(EncryptHelper.encrypt(p));

                Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://wine.dynamicwebapac.com")
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();

                DynamicWebService service = retrofit.create(DynamicWebService.class);
                Call<LoginResponse> call = service.authenticateUser(loginRequest);
                call.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Response<LoginResponse> response, Retrofit retrofit) {
                        dia.dismiss();
                        setResult(RESULT_OK);
                        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = settings.edit();
                        Log.d("success", "success");
                        editor.putString("session", response.body().getSessionId());
                        Log.d("session id", response.body().getSessionId());
                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
                        i.addFlags(67108864);
                        startActivity(i);
                        finish();
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        dia.dismiss();
                        Utils.showDialog(
                                LoginActivity.this,
                                getString(R.string.err_login));
                        Log.d("cause", t.toString());

                    }
                });
            } catch (Exception e) {
                dia.dismiss();
                Utils.showDialog(
                        LoginActivity.this,
                        getString(R.string.err_login));
            }

        } else if (v.getId() == R.id.btnReg) {
            startActivityForResult(new Intent(this, RegisterOneActivity.class), 10);
        } else if (v.getId() == R.id.btnForget) {
            startActivityForResult(new Intent(this, ForgotIdActivity.class), 10);
        }

    }
}
