package com.dynamicwebapac.commerce.mobile.utils;

import android.util.Log;
import com.dynamicwebapac.commerce.mobile.crypto.Aes;

/**
 * Created by Sandah Aung on 5/11/15.
 */
public class EncryptHelper {
    public static String encrypt(String text) {
        try {
            Aes _crypt = new Aes();
            String output= "";
            String plainText = text;
            String key = Aes.SHA256("dwapacmm", 31); //32 bytes = 256 bit
            String iv = "dwdevelopmentsmm"; //16 bytes = 128 bit
            output = _crypt.encrypt(plainText, key, iv); //encrypt
            Log.d("login text:", output);
            return output.trim();
        } catch (Exception e) {

        }
        return "";
    }

    public static String decrypt(String text) {
        try {
            Aes _crypt = new Aes();
            String output= "";
            String plainText = text;
            String key = Aes.SHA256("dwapacmm", 31); //32 bytes = 256 bit
            String iv = "dwdevelopmentsmm"; //16 bytes = 128 bit
            output = _crypt.decrypt(plainText, key, iv); //encrypt
            return output.trim();
        } catch (Exception e) {

        }
        return "";
    }
}
