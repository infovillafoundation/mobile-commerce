package com.dynamicwebapac.commerce.mobile.activities;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import com.dynamicwebapac.commerce.mobile.R;
import com.dynamicwebapac.commerce.mobile.custom.CustomActivity;
import com.dynamicwebapac.commerce.mobile.custom.CustomFragment;
import com.dynamicwebapac.commerce.mobile.models.Data;
import com.dynamicwebapac.commerce.mobile.ui.*;

import java.util.ArrayList;

@SuppressLint({"InlinedApi"})
public class MainActivity extends CustomActivity {
    private static final TypeEvaluator ARGB_EVALUATOR;
    private DrawerLayout drawerLayout;
    private ListView drawerLeft;
    private ActionBarDrawerToggle drawerToggle;
    private boolean mActionBarAutoHideEnabled;
    private int mActionBarAutoHideMinY;
    private int mActionBarAutoHideSensivity;
    private int mActionBarAutoHideSignal;
    private boolean mActionBarShown;
    private ObjectAnimator mStatusBarColorAnimator;
    public Toolbar toolbar;

    class C01383 implements AnimatorUpdateListener {
        private final boolean shown;

        C01383(boolean z) {
            this.shown = z;
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            ViewCompat.postInvalidateOnAnimation(MainActivity.this.drawerLayout);
            if (this.shown) {
                MainActivity.this.getSupportActionBar().show();
                return;
            }
            MainActivity.this.getSupportActionBar().hide();
        }
    }

    class C01395 implements OnItemClickListener {
        private final LeftNavAdapter val$adp;

        C01395(LeftNavAdapter leftNavAdapter) {
            this.val$adp = leftNavAdapter;
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
            if (pos != 0) {
                this.val$adp.setSelection(pos - 1);
            }
            MainActivity.this.drawerLayout.closeDrawers();
            MainActivity.this.setupContainer(pos);
        }
    }

    class C01862 implements DrawerListener {
        C01862() {
        }

        public void onDrawerClosed(View drawerView) {
            MainActivity.this.onNavDrawerStateChanged(false, false);
        }

        public void onDrawerOpened(View drawerView) {
            MainActivity.this.onNavDrawerStateChanged(true, false);
        }

        public void onDrawerStateChanged(int newState) {
            MainActivity.this.onNavDrawerStateChanged(MainActivity.this.drawerLayout.isDrawerOpen(GravityCompat.START), newState != DrawerLayout. STATE_IDLE );
        }

        public void onDrawerSlide(View drawerView, float slideOffset) {
        }
    }

    class C01874 extends OnScrollListener {
        static final int ITEMS_THRESHOLD = 3;
        int lastFvi;

        C01874() {
            this.lastFvi = 0;
        }

        public void onScrollStateChanged(RecyclerView view, int scrollState) {
        }

        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            int i = 0;
            super.onScrolled(recyclerView, dx, dy);
            try {
                int i2;
                int firstVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                MainActivity mainActivity = MainActivity.this;
                if (firstVisibleItem <= ITEMS_THRESHOLD) {
                    i2 = 0;
                } else {
                    i2 = 0x7fffffff;
                }
                if (this.lastFvi - firstVisibleItem > 0) {
                    i = LinearLayoutManager.INVALID_OFFSET;
                } else if (this.lastFvi != firstVisibleItem) {
                    i = 0x7fffffff;
                }
                mainActivity.onMainContentScrolled(i2, i);
                this.lastFvi = firstVisibleItem;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class C02031 extends ActionBarDrawerToggle {
        C02031(Activity $anonymous0, DrawerLayout $anonymous1, int $anonymous2, int $anonymous3) {
            super($anonymous0, $anonymous1, $anonymous2, $anonymous3);
        }

        public void onDrawerClosed(View view) {
        }

        public void onDrawerOpened(View drawerView) {
        }
    }

    public MainActivity() {
        this.mActionBarAutoHideEnabled = true;
        this.mActionBarShown = true;
        this.mActionBarAutoHideSensivity = 0;
        this.mActionBarAutoHideMinY = 0;
        this.mActionBarAutoHideSignal = 0;
    }

    static {
        ARGB_EVALUATOR = new ArgbEvaluator();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(this.toolbar);
        setupDrawer();
        setupContainer(1);
    }

    private void setupDrawer() {
        this.drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        this.drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        this.drawerToggle = new C02031(this, this.drawerLayout, R.string.drawer_open, R.string.drawer_close);
        this.drawerLayout.setDrawerListener(this.drawerToggle);
        this.drawerLayout.closeDrawers();
        setupLeftNavDrawer();
        this.drawerLayout.setDrawerListener(new C01862());
    }

    private void onNavDrawerStateChanged(boolean isOpen, boolean isAnimating) {
        if (this.mActionBarAutoHideEnabled && isOpen) {
            autoShowOrHideActionBar(true);
        }
    }

    private void autoShowOrHideActionBar(boolean show) {
        if (show != this.mActionBarShown) {
            this.mActionBarShown = show;
            onActionBarAutoShowOrHide(show);
        }
    }

    @SuppressLint({"NewApi"})
    private void onActionBarAutoShowOrHide(boolean shown) {
        String str;
        int color;
        if (this.mStatusBarColorAnimator != null) {
            this.mStatusBarColorAnimator.cancel();
        }
        DrawerLayout drawerLayout = this.drawerLayout;
        if (this.drawerLayout != null) {
            str = "statusBarBackgroundColor";
        } else {
            str = "statusBarColor";
        }
        int[] iArr = new int[2];
        if (shown) {
            color = getResources().getColor(R.color.main_color_dk);
        } else {
            color = getResources().getColor(R.color.main_color);
        }
        iArr[0] = color;
        if (shown) {
            color = getResources().getColor(R.color.main_color);
        } else {
            color = getResources().getColor(R.color.main_color_dk);
        }
        iArr[1] = color;
        this.mStatusBarColorAnimator = ObjectAnimator.ofInt(drawerLayout, str, iArr).setDuration(250);
        if (this.drawerLayout != null) {
            this.mStatusBarColorAnimator.addUpdateListener(new C01383(shown));
        }
        this.mStatusBarColorAnimator.setEvaluator(ARGB_EVALUATOR);
        this.mStatusBarColorAnimator.start();
    }

    public void enableActionBarAutoHide(RecyclerView recList) {
        initActionBarAutoHide();
        recList.setOnScrollListener(new C01874());
    }

    private void onMainContentScrolled(int currentY, int deltaY) {
        if (deltaY > this.mActionBarAutoHideSensivity) {
            deltaY = this.mActionBarAutoHideSensivity;
        } else if (deltaY < (-this.mActionBarAutoHideSensivity)) {
            deltaY = -this.mActionBarAutoHideSensivity;
        }
        if (Math.signum((float) deltaY) * Math.signum((float) this.mActionBarAutoHideSignal) < 0.0f) {
            this.mActionBarAutoHideSignal = deltaY;
        } else {
            this.mActionBarAutoHideSignal += deltaY;
        }
        boolean shouldShow = currentY < this.mActionBarAutoHideMinY || this.mActionBarAutoHideSignal <= (-this.mActionBarAutoHideSensivity);
        autoShowOrHideActionBar(shouldShow);
    }

    public void initActionBarAutoHide() {
        this.mActionBarAutoHideEnabled = true;
        this.mActionBarAutoHideMinY = getResources().getDimensionPixelSize(R.dimen.action_bar_auto_hide_min_y);
        this.mActionBarAutoHideSensivity = getResources().getDimensionPixelSize(R.dimen.action_bar_auto_hide_sensivity);
    }

    @SuppressLint({"InflateParams"})
    private void setupLeftNavDrawer() {
        this.drawerLeft = (ListView) findViewById(R.id.left_drawer);
        this.drawerLeft.addHeaderView(getLayoutInflater().inflate(R.layout.left_nav_header, null));
        ArrayList<Data> al = new ArrayList();
        al.add(new Data(new String[]{"Explore"}, new int[]{R.drawable.ic_nav1, R.drawable.ic_nav1_sel}));
        al.add(new Data(new String[]{"Favourites"}, new int[]{R.drawable.ic_nav2, R.drawable.ic_nav2_sel}));
        al.add(new Data(new String[]{"Cart"}, new int[]{R.drawable.ic_nav3, R.drawable.ic_nav3_sel}));
        al.add(new Data(new String[]{"Settings"}, new int[]{R.drawable.ic_nav4, R.drawable.ic_nav4_sel}));
        al.add(new Data(new String[]{"Logout"}, new int[]{R.drawable.ic_nav5, R.drawable.ic_nav5_sel}));
        LeftNavAdapter adp = new LeftNavAdapter(this, al);
        this.drawerLeft.setAdapter(adp);
        this.drawerLeft.setOnItemClickListener(new C01395(adp));
    }

    private void setupContainer(int pos) {
        CustomFragment f = null;
        CharSequence title = null;
        if (pos != 0) {
            if (pos == 1) {
                f = new MainFragment();
            } else if (pos == 2) {
                f = new OnSale();
                title = "On Sale";
            } else if (pos == 3) {
                f = new Checkout();
                title = "Checkout";
            } else if (pos == 4) {
                f = new Settings();
                title = "Settings";
            } else if (pos == 5) {
                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = settings.edit();
                editor.remove("session");
                startActivity(new Intent(this, LoginActivity.class));
                finish();
            }
            if (f != null) {
                getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, f).commit();
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setTitle(title);
                }
            }
        }
    }

    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        this.drawerToggle.syncState();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        this.drawerToggle.onConfigurationChanged(newConfig);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (this.drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
