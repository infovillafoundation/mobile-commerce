package com.dynamicwebapac.commerce.mobile.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import com.dynamicwebapac.commerce.mobile.R;


public class SplashScreenActivity extends Activity {

    private boolean isRunning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        this.isRunning = true;
        startSplash();
    }

    private void startSplash() {
        new Thread(new C01421()).start();
    }

    private synchronized void doFinish() {
        if (this.isRunning) {
            this.isRunning = false;
            Intent i = new Intent(this, HomeActivity.class);
            i.addFlags(67108864);
            startActivity(i);
            finish();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.isRunning = false;
        finish();
        return true;
    }

    class C01421 implements Runnable {

        class C01411 implements Runnable {
            C01411() {
            }

            public void run() {
                SplashScreenActivity.this.doFinish();
            }
        }

        C01421() {
        }

        public void run() {
            try {
                Thread.sleep(3000);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                SplashScreenActivity.this.runOnUiThread(new C01411());
            }
        }
    }
}
