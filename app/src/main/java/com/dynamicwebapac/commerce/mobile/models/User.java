package com.dynamicwebapac.commerce.mobile.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandah Aung on 1/11/15.
 */

public class User {

    @SerializedName("User")
    private UserType userType;

    public User() {
        userType = new UserType();
    }

    public int getUserId() {
        return userType.getUserId();
    }

    public void setUserId(int userId) {
        userType.setUserId(userId);
    }

    public String getFirstName() {
        return userType.getFirstName();
    }

    public void setFirstName(String firstName) {
        userType.setFirstName(firstName);
    }

    public String getLastName() {
        return userType.getLastName();
    }

    public void setLastName(String lastName) {
        userType.setLastName(lastName);
    }

    public String getUsername() {
        return userType.getUsername();
    }

    public void setUsername(String username) {
        userType.setUsername(username);
    }

    public String getDisplayName() {
        return userType.getDisplayName();
    }

    public void setDisplayName(String displayName) {
        userType.setDisplayName(displayName);
    }

    public String getEmail() {
        return userType.getEmail();
    }

    public void setEmail(String email) {
        userType.setEmail(email);
    }

    public String getMobile() {
        return userType.getMobile();
    }

    public void setMobile(String mobile) {
        userType.setMobile(mobile);
    }

    public String getPassword() {
        return userType.getPassword();
    }

    public void setPassword(String password) {
        userType.setPassword(password);
    }
}
