package com.dynamicwebapac.commerce.mobile.custom;

import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import com.dynamicwebapac.commerce.mobile.utils.TouchEffect;


public class RegisterCustomActivity extends FragmentActivity implements OnClickListener {

	public static final TouchEffect TOUCH = new TouchEffect();

	@Override
	public void setContentView(int layoutResID)
	{
		super.setContentView(layoutResID);
	}


	public View setTouchNClick(int id)
	{

		View v = setClick(id);
		if (v != null)
			v.setOnTouchListener(TOUCH);
		return v;
	}

	public View setClick(int id)
	{

		View v = findViewById(id);
		if (v != null)
			v.setOnClickListener(this);
		return v;
	}
	
	public void onClick(View v)
	{

	}
}
