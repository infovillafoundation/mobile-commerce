package com.dynamicwebapac.commerce.mobile.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandah Aung on 4/11/15.
 */
public class LoginRequest {

    @SerializedName("Username")
    private String username;

    @SerializedName("Password")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
