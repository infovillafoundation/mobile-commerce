package com.dynamicwebapac.commerce.mobile.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.dynamicwebapac.commerce.mobile.R;
import com.dynamicwebapac.commerce.mobile.custom.RegisterCustomActivity;
import com.dynamicwebapac.commerce.mobile.models.RegisterOneModel;
import com.dynamicwebapac.commerce.mobile.models.User;
import com.dynamicwebapac.commerce.mobile.rest.DynamicWebService;
import com.dynamicwebapac.commerce.mobile.utils.Utils;
import retrofit.*;


public class RegisterTwoActivity extends RegisterCustomActivity {

    private EditText username;
    private EditText displayName;
    private EditText password;

    private static RegisterOneModel registerOneModel = RegisterOneActivity.registerOneModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_2);

        setTouchNClick(R.id.btnReg);

        username = (EditText) findViewById(R.id.username);
        displayName = (EditText) findViewById(R.id.display_name);
        password = (EditText) findViewById(R.id.password);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        String u = username.getText().toString();
        String d = displayName.getText().toString();
        String p = password.getText().toString();
        if (u.length() == 0 || d.length() == 0 || p.length() == 0)
        {
            Utils.showDialog(this, R.string.err_fields_empty);
            return;
        }
        final ProgressDialog dia = ProgressDialog.show(this, null,
                getString(R.string.alert_wait));
        String f = registerOneModel.getFirstName();
        String l = registerOneModel.getLastName();
        String e = registerOneModel.getEmail();
        String m = registerOneModel.getMobile();

        User user = new User();
        user.setFirstName(f);
        user.setLastName(l);
        user.setEmail(e);
        user.setMobile(m);
        user.setDisplayName(d);
        user.setPassword(p);
        user.setUsername(u);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://wine.dynamicwebapac.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        DynamicWebService service = retrofit.create(DynamicWebService.class);
        Call<User> call = service.createUser(user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Response<User> response, Retrofit retrofit) {
                dia.dismiss();
                setResult(RESULT_OK);
                RegisterOneActivity.registerOneModel = null;
                Toast.makeText(RegisterTwoActivity.this, R.string.reg_suc, Toast.LENGTH_SHORT).show();
                Intent i = new Intent(RegisterTwoActivity.this, LoginActivity.class);
                i.addFlags(67108864);
                startActivity(i);
                finish();
            }

            @Override
            public void onFailure(Throwable t) {
                dia.dismiss();
                Utils.showDialog(
                        RegisterTwoActivity.this,
                        getString(R.string.err_singup));

            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

