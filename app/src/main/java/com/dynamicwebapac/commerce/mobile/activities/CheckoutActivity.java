package com.dynamicwebapac.commerce.mobile.activities;

import android.os.Bundle;
import com.dynamicwebapac.commerce.mobile.R;
import com.dynamicwebapac.commerce.mobile.custom.CustomActivity;

public class CheckoutActivity extends CustomActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_checkout);
        getSupportActionBar().setTitle((CharSequence) "Checkout");
    }
}
