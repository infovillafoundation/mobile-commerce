package com.dynamicwebapac.commerce.mobile.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.dynamicwebapac.commerce.mobile.R;
import com.dynamicwebapac.commerce.mobile.custom.RegisterCustomActivity;
import com.dynamicwebapac.commerce.mobile.models.RegisterOneModel;


public class ForgotIdActivity extends RegisterCustomActivity {

    public static RegisterOneModel registerOneModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_id);

        setTouchNClick(R.id.btnUser);
        setTouchNClick(R.id.btnPwd);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        if (v.getId() == R.id.btnUser) {
            startActivityForResult(new Intent(this, RegisterOneActivity.class), 10);
        } else if (v.getId() == R.id.btnPwd) {
            startActivityForResult(new Intent(this, ForgotIdActivity.class), 10);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            finish();
        }
    }
}

