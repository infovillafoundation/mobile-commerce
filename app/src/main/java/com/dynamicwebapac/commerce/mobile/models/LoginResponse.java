package com.dynamicwebapac.commerce.mobile.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandah Aung on 4/11/15.
 */
public class LoginResponse {

    @SerializedName("UserId")
    private String userId;

    @SerializedName("UserName")
    private String username;

    @SerializedName("SessionId")
    private String sessionId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
