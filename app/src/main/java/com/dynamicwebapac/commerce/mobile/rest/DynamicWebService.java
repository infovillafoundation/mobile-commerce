package com.dynamicwebapac.commerce.mobile.rest;

import com.dynamicwebapac.commerce.mobile.models.LoginRequest;
import com.dynamicwebapac.commerce.mobile.models.LoginResponse;
import com.dynamicwebapac.commerce.mobile.models.User;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

/**
 * Created by Sandah Aung on 1/11/15.
 */


public interface DynamicWebService {
    @Headers("X-ApiKey: somelongrandomkey")
    @POST("/restservice/register?format=json")
    Call<User> createUser(@Body User user);

    @Headers("X-ApiKey: somelongrandomkey")
    @POST("/restservice//authenticate?format=json")
    Call<LoginResponse> authenticateUser(@Body LoginRequest loginRequest);
}
