package com.dynamicwebapac.commerce.mobile.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.dynamicwebapac.commerce.mobile.R;
import com.dynamicwebapac.commerce.mobile.activities.MainActivity;
import com.dynamicwebapac.commerce.mobile.custom.CustomFragment;
import com.dynamicwebapac.commerce.mobile.models.Data;

import java.util.ArrayList;

public class Settings extends CustomFragment {
    private ArrayList<Data> iList;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.settings, null);
        ((MainActivity) getActivity()).toolbar.setTitle((CharSequence) "Settings");
        ((MainActivity) getActivity()).toolbar.findViewById(R.id.spinner_toolbar).setVisibility(8);
        return v;
    }

    public void onClick(View v) {
        super.onClick(v);
    }
}
