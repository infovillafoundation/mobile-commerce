package com.dynamicwebapac.commerce.mobile.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import com.dynamicwebapac.commerce.mobile.R;
import com.dynamicwebapac.commerce.mobile.custom.RegisterCustomActivity;
import com.dynamicwebapac.commerce.mobile.models.RegisterOneModel;
import com.dynamicwebapac.commerce.mobile.utils.Utils;


public class RegisterOneActivity extends RegisterCustomActivity {

    private EditText firstName;
    private EditText lastName;
    private EditText email;
    private EditText mobile;

    public static RegisterOneModel registerOneModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_1);

        setTouchNClick(R.id.btnNxt);

        firstName = (EditText) findViewById(R.id.first_name);
        lastName = (EditText) findViewById(R.id.last_name);
        email = (EditText) findViewById(R.id.email);
        mobile = (EditText) findViewById(R.id.mobile);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        String f = firstName.getText().toString();
        String l = lastName.getText().toString();
        String e = email.getText().toString();
        String m = mobile.getText().toString();
        if (f.length() == 0 || l.length() == 0 || e.length() == 0 || m.length() == 0)
        {
            Utils.showDialog(this, R.string.err_fields_empty);
            return;
        } else {
            registerOneModel = new RegisterOneModel();
            registerOneModel.setFirstName(f);
            registerOneModel.setLastName(l);
            registerOneModel.setEmail(e);
            registerOneModel.setMobile(m);
            startActivityForResult(new Intent(this, RegisterTwoActivity.class), 15);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            finish();
        }
    }
}

