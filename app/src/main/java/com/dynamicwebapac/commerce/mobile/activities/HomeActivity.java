package com.dynamicwebapac.commerce.mobile.activities;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.dynamicwebapac.commerce.mobile.R;
import com.dynamicwebapac.commerce.mobile.custom.CustomActivity;


public class HomeActivity extends CustomActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setupView();
    }

    private void setupView() {
        setTouchNClick(R.id.btnReg);
        setTouchNClick(R.id.btnLogin);
    }

    public void onClick(View v) {
        super.onClick(v);
        if (v.getId() == R.id.btnLogin) {
            Intent i = new Intent(this, LoginActivity.class);
            i.addFlags(67108864);
            startActivity(i);
            finish();
        } else if (v.getId() == R.id.btnReg) {
            startActivityForResult(new Intent(this, RegisterOneActivity.class), 10);
            finish();
        }
    }
}

